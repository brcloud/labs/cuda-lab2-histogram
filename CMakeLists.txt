cmake_minimum_required(VERSION 2.8)
project(histogram)

find_package(CUDA REQUIRED)

add_executable(histogram src/histogram.c)
target_link_libraries(histogram m)

#cuda_add_executable(histogram-gpu src/histogram_gpu.cu)
